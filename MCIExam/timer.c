#include "LPC17xx.h"
#include "timer.h"
#include <stdio.h>

volatile int match0=0,match1=0,match2=0,match3=0;


void timer_init(uint32_t ms)
{

	uint32_t count_3sec;
	uint32_t count_7sec;
	uint32_t count_10sec;
	uint32_t count_12sec;
	LPC_SC->PCONP |= BV(PCON_PCTIM3);
	//For 3 sec interrupt 

	//disable clock and reset time registers
	LPC_TIM3->TCR = BV(TCR_RST);
	//timer mode
	LPC_TIM3->CTCR = CCR_TIMER;
	//prescalar
	LPC_TIM3->PR = TIM3_PR_VAL-1;

	count_3sec = (PCLK/1000)*ms/18;
	//assigning count value in MR0
	LPC_TIM3->MR0 = count_3sec -1;
	LPC_TIM3->MCR |= BV(MCR_MR0_I) |BV( MCR_MR0_R);

	ms = ms + 4;
	count_7sec = (PCLK/1000)*ms/18;
	//assigning count value in MR1
	LPC_TIM3->MR1 = count_7sec -1;
	LPC_TIM3->MCR |= BV(MCR_MR1_I) |BV(MCR_MR1_R);

	//For 10 sec interrupt
	ms = ms + 3;
	count_10sec = (PCLK/1000)*ms/18;
	//assigning count value in MR2
	LPC_TIM3->MR2 = count_10sec -1;
	LPC_TIM3->MCR |= BV(MCR_MR2_I) |BV(MCR_MR2_R);

	//For 12 sec interrupt
	ms = ms + 2;
	count_12sec = (PCLK/1000)*ms/18;
	//assigning count value in MR3
	LPC_TIM3->MR3 = count_12sec -1;
	LPC_TIM3->MCR |= BV(MCR_MR3_I) |BV(MCR_MR3_R);

	// enable interrupt
	NVIC_EnableIRQ(TIMER3_IRQn);
	// enable timer clock
	LPC_TIM3->TCR = BV(TCR_CLKEN);

}
void TIMER3_IRQHandler(void)
{
	match0 = 1;
	LPC_TIM3->IR = BV(EMR_MR0);
	match1 = 1;
	LPC_TIM3->IR = BV(EMR_MR1);
	match2 = 1;
	LPC_TIM3->IR = BV(EMR_MR2);
	match3 = 1;
	LPC_TIM3->IR = BV(EMR_MR3);
}
