#ifndef __SPI_H
#define __SPI_H

#include "LPC17xx.h"

#define SSEL				16

#define SSPCR0_8_BIT_DL		0x07
#define SSPCR0_FF0			4
#define SSPCR0_FF1			5
#define SSPCR0_CPOL			6
#define SSPCR0_CPHA			7
#define SSPCR0_SCR			8

#define SSPCR0_SCR_VAL		18

#define SSP_PR_VAL			2

#define SSPCR1_EN			1

#define SPSR_RXFNE			2

void spi_init(void);
uint32_t spi_transfer(uint32_t data);

#endif
