#include "LPC17xx.h"
#include "lcd.h"
#include "timer.h"
#include "spi.h"
#include <stdio.h>

int main()
{
	uint8_t val = 0x55;
	char str[20];

	int count3 = 0;
	int count7 = 0;
	int count10 = 0;
	int count12 = 0;
	lcd_init();
	lcd_puts(LCD_LINE1, "Navin Kukreja E4");
	delay_ms(2000);
	lcd_write_cmd(LCD_CLEAR);

	spi_init();
	timer_init(3000);
	while(1) {
		// wait for timer3 Match0 register interrupt
		while(match0 == 0)
			;
		match0 = 0;
		spi_transfer(val);
		val = ~val;
		++count3;
		lcd_puts(LCD_LINE1, "Count 3sec = 01");
		sprintf(str,"Count 3sec=%02d",count3);
		lcd_puts(LCD_LINE2, "3 = THREE");
		delay_ms(1000);
		lcd_write_cmd(LCD_CLEAR);

		// wait for timer3 Match1 register interrupt
		while(match1 == 0)
			;
		match1 = 0;
		spi_transfer(val);
		val = ~val;
		++count7;
		
		lcd_puts(LCD_LINE1, "Count 7sec = 01");
		sprintf(str,"Count 7sec=%02d",count7);
		lcd_puts(LCD_LINE2, "7 = Seven");
		delay_ms(1000);
		lcd_write_cmd(LCD_CLEAR);

		// wait for timer3 Match2 register interrupt
		while(match2 == 0)
			;
		match2 = 0;
		spi_transfer(val);
		val = ~val;
		++count10;
		lcd_puts(LCD_LINE1, "Count 10sec = 01");
		sprintf(str,"Count 10sec=%02d",count10);
		lcd_puts(LCD_LINE2, "10 = TEN");
		delay_ms(1000);
		lcd_write_cmd(LCD_CLEAR);


		// wait for timer3 Match3 register interrupt
		while(match3 == 0)
			;
		match3 = 0;
		spi_transfer(val);
		val = ~val;
		++count12;
		lcd_puts(LCD_LINE1, "Count 12sec = 01");
		sprintf(str,"Count 12sec=%02d",count12);
		lcd_puts(LCD_LINE2, "12 = TWELVE");
		delay_ms(1000);
		lcd_write_cmd(LCD_CLEAR);

	}
	return 0;
}

