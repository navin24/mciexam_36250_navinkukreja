#ifndef __TIMER_H
#define __TIMER_H

#include "LPC17xx.h"

#define PCON_PCTIM3 23

//enabling as a timer mode
#define CCR_TIMER 0x00

#define TCR_CLKEN		0
#define TCR_RST			1

//match registers
#define MCR_MR0_I			0
#define MCR_MR0_R			1
#define MCR_MR0_S			2
#define MCR_MR1_I			3
#define MCR_MR1_R			4
#define MCR_MR1_S			5
#define MCR_MR2_I			6
#define MCR_MR2_R			7
#define MCR_MR2_S			8
#define MCR_MR3_I			9
#define MCR_MR3_R			10
#define MCR_MR3_S			11

//match interrupts
#define EMR_MR0		0
#define EMR_MR1		1
#define EMR_MR2		2
#define EMR_MR3		3

#define TIM3_PR_VAL  18
//variable declarations
extern volatile int match0,match1,match2,match3;
extern  int count3,count7,count10,count12;
//functions
void timer_init(uint32_t delay_ms);
void TIMER3_IRQHandler(void);   

#endif
